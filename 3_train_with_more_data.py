from collections import defaultdict
import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
import doc_embeddings
import delete_quotes as dq
import features

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-itrain','--input-file', help='XML train file', required=True)
	parser.add_argument('-otrain','--output-file', help='Cleaned output train file', required=True)
	parser.add_argument('-ltrain','--label-file', help='Label train file', required=False)

	parser.add_argument('-hypextra','--hyperpartisan-extra', help='Additional hyperpartisan train data', required=True)
	parser.add_argument('-neutralextra','--neutral-extra', help='Additional neutral train data', required=False)

	parser.add_argument('-wv','--word-vectors', help='Word vectors', required=True)

	parser.add_argument('-mxw','--maxlen', help='Maximum doc length (in words)', required=True)
	parser.add_argument('-t','--tokenizer-path', help='Tokenizer path', required=True)
	parser.add_argument('-m','--model', help='Model path', required=True)

	args=parser.parse_args()

	# LOAD EXTRA DATA
	# lead extra hyper
	print('Loading new hyperpartisan data')
	new_hyp=defaultdict(str)
	c=0
	with open(args.hyperpartisan_extra,'r') as f:
		for line in f:
			new_hyp[c]+=line
			if line=='\n':
				c+=1
	print('Loading new neutral data')
	# load extra neutral
	new_nohyp=defaultdict(str)
	c=0
	with open(args.neutral_extra,'r') as f:
		for line in f:
			new_nohyp[c]+= line
			if line=='\n':
				c+=1

	# merge all
	print('loaded ',len(new_hyp),' hyp and ',len(new_nohyp),' neutral new documents')
	for idx,doc in new_hyp.items():
		documents.append(doc)
		labels.append(1)
	for idx,doc in new_nohyp.items():
		documents.append(doc)
		labels.append(0)

	documents,labels=shuffle(documents,labels)

	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(args.word_vectors)

	print('Vectorizing all new data (hyperpartisan and neutral)')
	X_train=[]
	for document in documents:
		doc_emb=doc_embeddings.article2emb(document, modelwords)
		features_vec=features.count(document)
		X_train.append(np.concatenate([doc_emb,features_vec]))

	print('Loading test data (645)')
	# LOAD DATA
	documents_645,labels_645,_=pipeline.load_xml_files(args.input_file, args.output_file, args.label_file, delete_quotes=True)
	print('Vectorizing test data (645)')
	X_test:
	for document in documents_645:
		doc_emb=doc_embeddings.article2emb(document, modelwords)
		features_vec=features.count(document)
		X_test.append(np.concatenate([doc_emb,features_vec]))


	# crear clasificador(es) y entrenarlos con el X_train y evaluar en X_test (645)
	# create svm
	clf = SVC(kernel='linear').fit(X_train, labels)

	# create lr
	clf = LogisticRegression(solver='saga').fit(X_train, labels)

	# create lstm
	print('=== CBLSTM ===')
	print('Vectorizing training data')
	tokenizer, X_train, y_train = pipeline.vectorize(documents, labels, int(args.maxlen), args.tokenizer_path)
	print('Vectorizing test data (645)')
	_, X_test, y_test = pipeline.vectorize(documents_645, labels_645, int(args.maxlen), args.tokenizer_path)
	print('Making embedding layer')
	embedding_layer=pipeline.make_embedding_layer(tokenizer,dimwords,vocabwords,modelwords,int(args.maxlen))
	nnmodel = pipeline.build_model(embedding_layer)
	nnmodel.fit(X_train,y_train,epochs=10)
	preds_lstm = [np.argmax(i) for i in nnmodel.predict(X_test)]
	print(classification_report(labels_test,preds_lstm))

	"""
	print('Now save models to file')
	pickle.dump(clf_our,open('extra_data/clf_our.pkl','wb'),protocol=pickle.HIGHEST_PROTOCOL)
	pickle.dump(clf_docemb,open('extra_data/clf_docemb.pkl','wb'),protocol=pickle.HIGHEST_PROTOCOL)
	nnmodel.save('extra_data/cblstm_model')
	pickle.dump(clf_final,open('extra_data/clf_final.pkl','wb'),protocol=pickle.HIGHEST_PROTOCOL)
	"""