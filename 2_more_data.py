import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential, load_model
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
import doc_embeddings
import delete_quotes as dq
import features

def load_645():
	ifile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/pan19-hyperpartisan-news-detection-by-article-training-dataset-2018-11-22'
	ofile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/pan19-hyperpartisan-news-detection-by-article-training-dataset-2018-11-22/articles-training-byarticle-20181122.xml_clean.txt'
	ofile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/pan19-hyperpartisan-news-detection-by-article-training-dataset-2018-11-22/articles-training-byarticle-20181122.xml_clean.txt'
	lfile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/ground-truth-training-byarticle-20181122.xml'
	data_target,labels_target=article_processor(ifile_target,ofile_target,lfile_target)	
	all_articles_target=[]
	all_labels_target=[]
	for doc_id,document in data_target.items():
		all_articles_target.append(document)
		all_labels_target.append(labels_target[doc_id])
	return all_articles_target,all_labels_target

if __name__ == '__main__':

	parser = ArgumentParser()

	parser.add_argument('-wv','--word-vectors', help='Word vectors', required=True)
	parser.add_argument('-target','--target-dataset', help='Dataset (by publisher) to bootstrap', required=True,
		choices=['training','validation'])
	parser.add_argument('-mxw','--maxlen', help='Maximum doc length (in words)', required=True)
	parser.add_argument('-t','--tokenizer-path', help='Tokenizer path', required=True)

	args=parser.parse_args()

	print('loading our features classifier')
	clf_our = pickle.load(open('models/clf_our.pkl','rb'))
	print('loading doc emb classifier')
	clf_docemb = pickle.load(open('models/clf_docemb.pkl','rb'))
	print('loading cblstm classifier')
	nnmodel = load_model('models/cblstm_model')
	print('loading meta classifier')
	clf_meta = pickle.load(open('models/clf_meta.pkl','rb'))

	ifile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/pan19-hyperpartisan-news-detection-by-publisher-'+args.target_dataset+'-dataset-2018-11-22'
	ofile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/pan19-hyperpartisan-news-detection-by-publisher-'+args.target_dataset+'-dataset-2018-11-22/articles-'+args.target_dataset+'-bypublisher-20181122.xml_clean.txt'
	ofile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/pan19-hyperpartisan-news-detection-by-publisher-'+args.target_dataset+'-dataset-2018-11-22/articles-'+args.target_dataset+'-bypublisher-20181122.xml_clean.txt'
	lfile_target='/media/luis/2TB/resources/corpora/semeval_hyperpartisan/ground-truth-'+args.target_dataset+'-bypublisher-20181122.xml'

	print('=== Reading target Dataset: '+args.target_dataset+' ===\n')
	data_target,labels_target=article_processor(ifile_target,ofile_target,lfile_target)	
	all_articles_target=[]
	all_labels_target=[]
	for doc_id,document in data_target.items():
		all_articles_target.append(document)
		all_labels_target.append(labels_target[doc_id])

	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(args.word_vectors)
	
	print('=== Vectorizing (our) target dataset: '+args.target_dataset+' ===')
	X_our_target=[]
	for idx,document in enumerate(all_articles_target):
		features_vec=features.count(document)
		X_our_target.append(features_vec)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(all_articles_target))

	print('=== Vectorizing (docemb) target dataset: '+args.target_dataset+' ===')
	X_docemb_target=[]
	for idx,document in enumerate(all_articles_target):
		doc_emb=doc_embeddings.article2emb(document, modelwords)
		X_docemb_target.append(doc_emb)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(all_articles_target))

	print('=== Vectorizing target (embedding) dataset: '+args.target_dataset+' ===')
	### CBLSTM EXPERIMENT ###
	input('-- wait for more data (training) to finish to use that tokenizer!! --')
	tokenizer, X_lstm_target, y_lstm_target = pipeline.vectorize(all_articles_target, all_labels_target, int(args.maxlen), args.tokenizer_path)
	
	print('=== Applying trained model on target dataset ===')
	# prediction agrees with dataset label
	predicted_hyperpartisan=[] 
	predicted_no_hyperpartisan=[]
	for idx,xi_our in enumerate(X_our_target):
		xi_docemb=X_docemb_target[idx]
		xi_lstm=X_lstm_target[idx]

		pred_our = clf_our.predict([xi_our])
		pred_docemb = clf_docemb.predict([xi_docemb])
		pred_lstm = nnmodel.predict(np.array([xi_lstm]))

		pred_meta = clf_meta.predict_proba([pred_our,pred_docemb,pred_lstm])

		# get prediction label and confidence
		predicted_class = np.argmax(pred_meta)
		predicted_conf = np.max(pred_meta)
		# add to gold-ish train data
		if all_labels_target[idx]==1:
			if predicted_class == 1:# and predicted_conf > 0.75:
				predicted_hyperpartisan.append(all_articles_target[idx])
				#print(pred_our,pred_docemb,pred_lstm,'-->',p)
				#input('--')
		else:
			if predicted_class == 0:# and predicted_conf > 0.75:
				#print(pred_our,pred_docemb,pred_lstm,'-->',p)
				predicted_no_hyperpartisan.append(all_articles_target[idx])
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(X_target),' | Size of hyper: ',len(predicted_hyperpartisan),' | Size of neutral: ',len(predicted_no_hyperpartisan))
		
	print('Saving additional data: ',len(predicted_hyperpartisan),' hyper and ',len(predicted_no_hyperpartisan),' neutral')
	with open('extra_data/extra_hyper_'+args.target_dataset+'_meta.txt','w') as outf:
		for doc in predicted_hyperpartisan:
			outf.write(doc)
			outf.write('\n')
	with open('extra_data/extra_nohyper_'+args.target_dataset+'_meta.txt','w') as outf:
		for doc in predicted_no_hyperpartisan:
			outf.write(doc)
			outf.write('\n')
	sys.exit('done!')