from collections import defaultdict
import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
import doc_embeddings
import delete_quotes as dq
import features

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-itrain','--input-file', help='XML TEST (645) file', required=True)
	parser.add_argument('-otrain','--output-file', help='Cleaned output TEST (645) file', required=True)
	parser.add_argument('-ltrain','--label-file', help='Label TEST (645) file', required=False)

	parser.add_argument('-hypextra','--hyperpartisan-extra', help='Additional hyperpartisan train data', required=True)
	parser.add_argument('-neutralextra','--neutral-extra', help='Additional neutral train data', required=False)

	parser.add_argument('-mxw','--maxlen', help='Maximum doc length (in words)', required=True)
	parser.add_argument('-t','--tokenizer-path', help='Tokenizer path', required=True)

	args=parser.parse_args()

	word_vectors = 'glove.6B.300d.txt_header.txt.bin'

	# LOAD EXTRA DATA
	# lead extra hyper
	new_hyp=defaultdict(str)
	c=0
	with open(args.hyperpartisan_extra,'r') as f:
		for line in f:
			new_hyp[c]+=line.lower()
			if line=='\n':
				c+=1
	# load extra neutral
	new_nohyp=defaultdict(str)
	c=0
	with open(args.neutral_extra,'r') as f:
		for line in f:
			new_nohyp[c]+= line.lower()
			if line=='\n':
				c+=1
	
	big_hyper_docs = []
	big_hyper_labels = []
	big_neutral_docs = []
	big_neutral_labels = []

	# merge all
	print('loaded ',len(new_hyp),' hyp and ',len(new_nohyp),' neutral new documents')
	for idx,doc in new_hyp.items():
		big_hyper_docs.append(doc)
		big_hyper_labels.append(1)
	for idx,doc in new_nohyp.items():
		big_neutral_docs.append(doc)
		big_neutral_labels.append(0)

	HYPER_LIMIT=100
	NEUTRAL_LIMIT=500

	big_hyper_docs,big_hyper_labels=big_hyper_docs[:HYPER_LIMIT],big_hyper_labels[:HYPER_LIMIT]
	big_neutral_docs,big_neutral_labels=big_neutral_docs[:NEUTRAL_LIMIT],big_neutral_labels[:NEUTRAL_LIMIT]

	print('Hyper data sizes: ',len(big_hyper_docs),len(big_hyper_labels))
	print('Neutral data sizes: ',len(big_neutral_docs),len(big_neutral_labels))

	documents=big_hyper_docs+big_neutral_docs
	labels=big_hyper_labels+big_neutral_labels	
	train_documents,train_labels=shuffle(documents,labels)
	
	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(word_vectors)

	### PREPROCESS TRAIN DATA ###
	print('=== Vectorizing (our) train dataset ===')
	X_our_train=[]
	for idx,document in enumerate(documents):
		features_vec=features.count(document)
		X_our_train.append(features_vec)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(documents))
	clf_our = SVC(kernel='linear').fit(X_our_train, labels)
	print('=== Vectorizing (docemb) train dataset ===')
	X_docemb_train=[]
	for idx,document in enumerate(documents):
		doc_emb=doc_embeddings.article2emb(document, modelwords)
		X_docemb_train.append(doc_emb)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(documents))
	clf_docemb = SVC(kernel='linear').fit(X_docemb_train, labels)
	print('=== Vectorizing (lstm) train dataset ===')
	tokenizer, X_train, y_train = pipeline.vectorize(documents, labels, int(args.maxlen), args.tokenizer_path)
	embedding_layer=pipeline.make_embedding_layer(tokenizer,dimwords,vocabwords,modelwords,int(args.maxlen))
	nnmodel = pipeline.build_model(embedding_layer)
	nnmodel.fit(X_train,y_train,epochs=10)

	### PREPROCESS TEST (645) DATA ###
	test_documents,test_labels,_=pipeline.load_xml_files(args.input_file, args.output_file, args.label_file, delete_quotes=True)
	print('=== Vectorizing (our) test 645 dataset ===')
	X_our_test=[]
	for idx,document in enumerate(test_documents):
		features_vec=features.count(document)
		X_our_test.append(features_vec)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(test_documents))

	print('=== Vectorizing (docemb) test 645 dataset ===')
	X_docemb_test=[]
	for idx,document in enumerate(test_documents):
		doc_emb=doc_embeddings.article2emb(document, modelwords)
		X_docemb_test.append(doc_emb)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(test_documents))

	print('=== Vectorizing (lstm) dataset ===')
	test_documents,test_labels=shuffle(test_documents,test_labels)
	tokenizer, X_test, y_test = pipeline.vectorize(test_documents, test_labels, int(args.maxlen), args.tokenizer_path)

	preds_our = clf_our.predict(X_our_test)
	preds_docemb = clf_docemb.predict(X_docemb_test)
	preds_lstm = [np.argmax(i) for i in nnmodel.predict(X_test)]

	X_train_meta=[]
	for idx,pred_our in enumerate(preds_our):
		pred_docemb=preds_docemb[idx]
		pred_lstm=preds_lstm[idx]
		xi=[pred_our,pred_docemb,pred_lstm]
		X_train_meta.append(xi)

	META_TRAIN_SPLIT=round(645/2)
	clf_meta = SVC(kernel='linear').fit(X_train_meta[:META_TRAIN_SPLIT], test_labels[:META_TRAIN_SPLIT])
	preds_meta=clf_meta.predict(X_train_meta[META_TRAIN_SPLIT:])
	print(classification_report(test_labels[META_TRAIN_SPLIT:],preds_meta))

	print('Now save (big) models to file')

	pickle.dump(clf_our,open('models/clf_our.pkl','wb'),protocol=pickle.HIGHEST_PROTOCOL)
	pickle.dump(clf_docemb,open('models/clf_docemb.pkl','wb'),protocol=pickle.HIGHEST_PROTOCOL)
	nnmodel.save('models/cblstm_model')
	pickle.dump(clf_meta,open('models/clf_meta.pkl','wb'),protocol=pickle.HIGHEST_PROTOCOL)
		