from collections import defaultdict
import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline_nokeras
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
import doc_embeddings
import delete_quotes as dq
import features
from keras.models import load_model
import pipeline

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-idir','--input-dir', help='XML file', required=True)
	parser.add_argument('-odir','--output-dir', help='Predictions dir', required=True)

	args=parser.parse_args()

	word_vectors = 'glove.6B.300d.txt_header.txt.bin'
	tokenizer_path = 'vectorizers/tokenizer'
	maxlen = 500

	print('loading our features classifier')
	clf_our = pickle.load(open('models/clf_our.pkl','rb'))

	print('loading doc emb classifier')
	clf_docemb = pickle.load(open('models/clf_docemb.pkl','rb'))

	print('loading cblstm classifier')
	nnmodel = load_model('models/cblstm_model')

	print('loading meta classifier')
	clf_meta = pickle.load(open('models/clf_meta.pkl','rb'))

	for k in args.input_dir.split('/'):
		if k.startswith('pan'):
			clean_dataset_name=k.replace('-','_')+'_clean.txt'

	# LOAD DATA
	documents,_,doc_ids=pipeline_nokeras.load_xml_files(args.input_dir, clean_dataset_name, lfile=False, delete_quotes=True)

	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(word_vectors)
	
	### OUR FEATURES + DOC EMBEDDINGS WITH SVM ###
	print('=== Vectorizing (our) ===')
	X_our=[]
	for idx,document in enumerate(documents):
		features_vec=features.count(document)
		X_our.append(features_vec)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(documents))

	print('=== Vectorizing (docemb) ===')
	X_docemb=[]
	for idx,document in enumerate(documents):
		doc_emb=doc_embeddings.article2emb(document, modelwords)
		X_docemb.append(doc_emb)
		if idx % 1000 == 0:
			print('Done ',idx,' of ',len(documents))

	print('=== Vectorizing (lstm) ===')
	dummy_label_array=[1,2,3]
	_, X_lstm, _ = pipeline.vectorize(documents, dummy_label_array, maxlen, tokenizer_path)


	preds_our = clf_our.predict(X_our)
	preds_docemb = clf_docemb.predict(X_docemb)
	preds_lstm = [np.argmax(i) for i in nnmodel.predict(X_lstm)]

	X_meta = []
	for idx,pred_our in enumerate(preds_our):
		pred_docemb=preds_docemb[idx]
		pred_lstm=preds_lstm[idx]
		xi=[pred_our,pred_docemb,pred_lstm]
		X_meta.append(xi)

	preds_meta = clf_meta.predict(X_meta)
	
	output_file = os.path.join(args.output_dir,'predictions_meta.txt')
	with open(output_file,'w') as outf:
		for idx,p in enumerate(preds_meta):
			doc_id = doc_ids[idx]
			if p == 1:
				pstr='true'
			else:
				pstr='false'
			outf.write(doc_id+'\t'+pstr+'\n')
		print('Predictions successfully written to: ',output_file)