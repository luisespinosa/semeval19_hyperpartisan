from collections import defaultdict
import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline_nokeras
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
import doc_embeddings
import delete_quotes as dq
import features

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-idir','--input-dir', help='XML file', required=True)
	parser.add_argument('-odir','--output-dir', help='Predictions dir', required=True)

	args=parser.parse_args()

	word_vectors = 'GoogleNews-vectors-negative300.bin.gz'
	combined_model = 'models/SVC_combined.clf'

	for k in args.input_dir.split('/'):
		if k.startswith('pan'):
			clean_dataset_name=k.replace('-','_')+'_clean.txt'

	# LOAD DATA
	documents,_,doc_ids=pipeline_nokeras.load_xml_files(args.input_dir, clean_dataset_name, lfile=False, delete_quotes=True)

	#clf_our = pickle.load(open(our_model,'rb'))
	clf = pickle.load(open(combined_model,'rb'))

	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(word_vectors)
	
	### OUR FEATURES + DOC EMBEDDINGS WITH SVM ###

	print('Vectorizing data')
	X=[]
	for doc in documents:
		doc_emb=doc_embeddings.article2emb(doc, modelwords)
		features_vec=features.count(doc)
		X.append(np.concatenate([doc_emb,features_vec]))

	X=np.array(X)

	print(X.shape)

	print('Predicting...')
	preds = clf.predict(X)
	
	output_file = os.path.join(args.output_dir,'predictions_combined.txt')
	with open(output_file,'w') as outf:
		for idx,p in enumerate(preds):
			doc_id = doc_ids[idx]
			if p == 1:
				pstr='true'
			else:
				pstr='false'
			outf.write(doc_id+'\t'+pstr+'\n')
		print('Predictions successfully written to: ',output_file)