

#We will apply this function to a dataset previously cleaned of quotes.

from nltk.tokenize import sent_tokenize
import numpy as np

def article2emb(string,model):
    article_emb=[]
    for sentence in sent_tokenize(string):
        sent_vectors=[]
        tokens=sentence.strip().split()
        for token in tokens:
            if token in model.vocab:
                wv=model[token]
                sent_vectors.append(wv)
        if sent_vectors:
            sent_emb = np.mean(sent_vectors, axis=0)
        else:
            sent_emb = np.zeros(model.vector_size)
        article_emb.append(sent_emb)
    doc_avg=np.mean(article_emb, axis=0)
    return doc_avg



# This code makes a list with all the doc_embeddings. Each element corresponds to an article.
'''
all_doc_embeddings=[]
for article in all_articles_no_quotes:
all_doc_embeddings.append(convert_article_to_doc_embedding(article))
'''