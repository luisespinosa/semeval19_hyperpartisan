
# This script contains the functions which will extract our selected features and the function which will 
# convert their outcomes in a vector of features.

import nltk
from nltk.tokenize import sent_tokenize

insults=[line.strip() for line in open('insults.txt', 'r')]
def count(article):
    excl,q,first,ins,adj,adv,avg_sent_len, min_sent, max_sent=0,0,0,0,0,0,0,0,0
    for tok,pos_tag in nltk.pos_tag(article.split()):
        if tok=='!':
            excl+=1
        elif tok=='?':
            q+=1
        elif tok.lower()=='i':
            first+=1
        elif tok in insults:
            ins+=1
        elif pos_tag=='JJ':
            adj+=1
        elif pos_tag=='RB':
            adv+=1
    sentences=sent_tokenize(article)
    sentence_lenghts=[]
    for sent in sentences:
        sentence_lenghts.append(len(sent.split()))
    avg_sent_len=sum(sentence_lenghts)/len(sentences)
    min_sent=min(sentence_lenghts)
    max_sent=max(sentence_lenghts)
    return [
    excl,
    q,
    first,
    ins,
    adj/len(article)*100,
    adv/len(article)*100,
    avg_sent_len,
    min_sent,
    max_sent
    ]   

def percentage_adjectives(tokenized_string):
    adj=0
    for token in nltk.pos_tag(tokenized_string):
        if token[1]=='JJ':
            adj=adj+1
    return (adj/len(tokenized_string))*100


def percentage_adverbs(tokenized_string):
    adv=0
    for token in nltk.pos_tag(tokenized_string):
        if token[1]=='RB':
            adv=adv+1
    return (adv/len(tokenized_string))*100


def count_exclamation (tokenized_string):
    exclamation=0
    for token in tokenized_string:
        if token=='!':
            exclamation=exclamation+1
    return exclamation


def count_question (tokenized_string):
    questions=0
    for token in tokenized_string:
        if token=='?':
            questions=questions+1
    return questions


def count_first_person(tokenized_string):
    first_person=0
    for token in tokenized_string:
        if token.lower()=='i':
            first_person=first_person+1
    return first_person

def count_insults(tokenized_article):
    insults=0
    for token in tokenized_article:
        if token in open('insults.txt', 'r'):
            insults=insults+1
    return insults



# To extract our features, the string must be cleaned of quotes:
#This function extracts our selected features from a string and returns a vector (list) with the results of each feature.

def extract_our_features(string):
    features=[]
    article=word_tokenize(string)
    features.append(count_exclamation(article))
    features.append(count_question(article))
    features.append(count_first_person(article))
    features.append(percentage_adjectives(article))
    features.append(percentage_adverbs(article))
    features.append(count_insults(article))
    return features