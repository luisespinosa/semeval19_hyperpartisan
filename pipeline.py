import sys
import os
from data_manager.read_dataset import *
from preprocess import preprocess_module
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import delete_quotes as dq

### CLEAN XML FILE ###
def load_xml_files(ifile, ofile, lfile=False, delete_quotes=True):
	print('=== Reading and Processing Data ===\n')
	data,train_labels=article_processor(ifile,ofile,lfile)
	all_train_articles=[]
	all_train_labels=[]
	for doc_id,document in data.items():
		if delete_quotes:
			all_train_articles.append(dq.delete_quotes(document.lower(), dq.QUOTATION_MARKS))
		else:
			all_train_articles.append(document)
		if train_labels:
			all_train_labels.append(train_labels[doc_id])
	return all_train_articles,all_train_labels,list(data.keys())

### VECTORIZE (CLEANED) DOCUMENTS ###
def vectorize(docs, labels, maxlen, tokenizer_path):
	tokenizer,X,y=preprocess(
		docs,
		labels,
		maxlen,
		tokenizer_path
		)
	return tokenizer,X,y

def preprocess(docs, labels, maxlen, tokenizer_path):
	"""
	If no tokenizer is provided, fit a tokenizer to the dataset (probably the train data)
	"""
	if not os.path.exists(tokenizer_path):
		tokenizer = Tokenizer()
		print('No tokenizer found, fitting to train set')
		tokenizer.fit_on_texts(docs)
		save_tokenizer(tokenizer, tokenizer_path)
	else:
		#print('Using tokenizer found at ',tokenizer_path)
		tokenizer = load_tokenizer(tokenizer_path)

	# convert raw docs to padded sequences of ints using the provided or trained tokenizer
	sequences = tokenizer.texts_to_sequences(docs)
	X = pad_sequences(sequences, maxlen=maxlen)
	# convert labels to one-hot encoded vectors
	y = to_categorical(np.asarray(labels))
	print('Shape of data tensor:', X.shape)
	print('Shape of label tensor:', y.shape)
	return tokenizer,X,y

### HELPERS TO SAVE AND LOAD KERAS TOKENIZER ###
def save_tokenizer(tokenizer, outpath):
	with open(outpath, 'wb') as handle:
	    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
	print('Tokenizer: ',tokenizer,' | Saved to: ',outpath)

def load_tokenizer(tokenizer_path):
	print('Loading tokenizer from: ',tokenizer_path)
	with open(tokenizer_path, 'rb') as handle:
		tokenizer = pickle.load(handle)
		return tokenizer

### MAKE EMBEDDING LAYER ###
def make_embedding_layer(tokenizer,vector_size,embedding_vocab,embedding_model,maxlen):
	# build word embedding layer based on dataset vocabulary
	word_index=tokenizer.word_index
	embedding_matrix = np.zeros((len(word_index)+1, vector_size))
	for word, index in word_index.items():
		if word in embedding_vocab:
			embedding_matrix[word_index[word]]=embedding_model[word]
	embedding_layer = Embedding(len(word_index) + 1,
								vector_size,
								weights=[embedding_matrix],
								input_length=maxlen,
								trainable=False)
	return embedding_layer

### BUILD CBLSTM MODEL ###
def build_model(embedding_layer):
	# Create cnn model
	filters=100
	kernel_size=5
	pool_size=4
	strides=3
	padding='valid'
	activation='relu'
	nnmodel = Sequential()
	nnmodel.add(embedding_layer)
	nnmodel.add(Conv1D(filters,
	                 kernel_size,
	                 padding=padding,
	                 activation=activation,
	                 strides=strides))
	nnmodel.add(MaxPooling1D(pool_size=pool_size))
	nnmodel.add(Bidirectional(LSTM(100)))
	nnmodel.add(Dropout(0.5))
	# We add a vanilla hidden layer:
	nnmodel.add(Dense(100))
	nnmodel.add(Dropout(0.5))
	nnmodel.add(Activation('relu'))	
	nnmodel.add(Dense(2))
	nnmodel.add(Activation('softmax'))
	nnmodel.compile(loss='binary_crossentropy',
	              optimizer='adagrad',
	              metrics=['accuracy'])
	print('=== Model Summary ===')
	print(nnmodel.summary())
	return nnmodel
