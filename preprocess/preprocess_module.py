import numpy as np
from data_manager.read_dataset import *
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
import pickle

def load_tokenizer(tokenizer_path):
	print('Loading tokenizer from: ',tokenizer_path)
	with open(tokenizer_path, 'rb') as handle:
		tokenizer = pickle.load(handle)
		return tokenizer

def save_tokenizer(tokenizer, outpath):
	with open(outpath, 'wb') as handle:
	    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
	print('Tokenizer: ',tokenizer,' | Saved to: ',outpath)

def preprocess(docs, labels, maxlen, tokenizer_path):
	"""
	If no tokenizer is provided, fit a tokenizer to the dataset (probably the train data)
	"""
	if not os.path.exists(tokenizer_path):
		tokenizer = Tokenizer()
		print('No tokenizer found, fitting to train set')
		tokenizer.fit_on_texts(docs)
	else:
		print('Using tokenizer found at ',tokenizer_path)
		tokenizer = load_tokenizer(tokenizer_path)
	# convert raw docs to padded sequences of ints using the provided or trained tokenizer
	sequences = tokenizer.texts_to_sequences(docs)
	X = pad_sequences(sequences, maxlen=maxlen)
	# convert labels to one-hot encoded vectors
	y = to_categorical(np.asarray(labels))
	print('Shape of data tensor:', X.shape)
	print('Shape of label tensor:', y.shape)
	return tokenizer,X,y

def vectorize(docs, labels, maxlen, tokenizer_path):
	tokenizer,X,y=preprocess(
		docs,
		labels,
		maxlen,
		tokenizer_path
		)
	return tokenizer,X,y

def make_embedding_layer(tokenizer,vector_size,embedding_vocab,embedding_model,maxlen):
	# build word embedding layer based on dataset vocabulary
	word_index=tokenizer.word_index
	embedding_matrix = np.zeros((len(word_index)+1, vector_size))
	for word, index in word_index.items():
		if word in embedding_vocab:
			embedding_matrix[word_index[word]]=embedding_model[word]
	embedding_layer = Embedding(len(word_index) + 1,
								vector_size,
								weights=[embedding_matrix],
								input_length=maxlen,
								trainable=False)
	return embedding_layer
