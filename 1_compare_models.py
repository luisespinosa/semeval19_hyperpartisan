import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
import doc_embeddings
import delete_quotes as dq
import features

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-itrain','--input-file', help='XML train file', required=True)
	parser.add_argument('-otrain','--output-file', help='Cleaned output train file', required=True)
	parser.add_argument('-ltrain','--label-file', help='Label train file', required=False)

	parser.add_argument('-wv','--word-vectors', help='Word vectors', required=True)

	parser.add_argument('-mxw','--maxlen', help='Maximum doc length (in words)', required=True)
	parser.add_argument('-t','--tokenizer-path', help='Tokenizer path', required=True)

	args=parser.parse_args()

	# LOAD DATA
	documents,labels,_=pipeline.load_xml_files(args.input_file, args.output_file, args.label_file, delete_quotes=True)	
	docs_train, docs_test, labels_train, labels_test = train_test_split(documents,labels,test_size=0.2,random_state=1)

	"""
	### OUR FEATURES CLASSIFIER ###
	X_train=[features.count(doc) for doc in docs_train]
	X_test=[features.count(doc) for doc in docs_test]
	clf = SVC(kernel='linear').fit(X_train, labels_train)
	preds_our = clf.predict(X_test)
	print(classification_report(labels_test,preds_our))

	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(args.word_vectors)

	### DOCUMENT EMBEDDINGS WITH SVM ###
	X_train=[doc_embeddings.article2emb(doc,modelwords) for doc in docs_train]
	X_test=[doc_embeddings.article2emb(doc,modelwords) for doc in docs_test]
	clf = SVC(kernel='linear').fit(X_train, labels_train)
	preds_docemb = clf.predict(X_test)
	print(classification_report(labels_test,preds_docemb))

	### CBLSTM EXPERIMENT ###
	tokenizer, X_train, y_train = pipeline.vectorize(docs_train, labels_train, int(args.maxlen), args.tokenizer_path)
	_, X_test, y_test = pipeline.vectorize(docs_test, labels_test, int(args.maxlen), args.tokenizer_path)
	embedding_layer=pipeline.make_embedding_layer(tokenizer,dimwords,vocabwords,modelwords,int(args.maxlen))
	nnmodel = pipeline.build_model(embedding_layer)
	nnmodel.fit(X_train,y_train,epochs=10)
	preds_lstm = nnmodel.predict(X_test)
	print('BLSTM')
	print(classification_report(labels_test,[np.argmax(i) for i in preds_lstm]))
	
	X_final=[]
	for i in range(len(preds_our)):
		array_preds = [preds_our[i], preds_docemb[i], np.argmax(preds_lstm[i])]
		X_final.append(array_preds)
	clf = SVC(kernel='linear',probability=True).fit(X_final[:-20], labels_test[:-20])
	preds_final = clf.predict(X_final[-20:])
	print(classification_report(labels_test[-20:],preds_final))
	"""