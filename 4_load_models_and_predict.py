from collections import defaultdict
import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
import doc_embeddings
import delete_quotes as dq
import features
from keras.models import load_model

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-ifile','--input-file', help='XML file', required=True)
	parser.add_argument('-ofile','--output-file', help='Cleaned output file', required=True)

	args=parser.parse_args()

	clean_ifile = 'clean_dataset.txt'
	word_vectors = '/media/luis/2TB/resources/embeddings/GoogleNews-vectors-negative300.bin.gz'
	maxlen = 500
	tokenizer_path = 'vectorizers/tokenizer'
	our_model = 'extra_data/clf_our.pkl'
	docemb_model = 'extra_data/clf_docemb.pkl'
	lstm_model = 'extra_data/cblstm_model'
	final_model = 'extra_data/clf_final.pkl'

	# LOAD DATA
	documents,_,doc_ids=pipeline.load_xml_files(args.input_file, clean_ifile, lfile=False, delete_quotes=True)	

	t = pipeline.load_tokenizer(tokenizer_path)
	print('loading model')
	nnmodel = load_model(lstm_model)
	print('vectorizing data')
	sequences = t.texts_to_sequences(documents)
	X = pad_sequences(sequences, maxlen=maxlen)
	print('Loaded dataset with shape: ',X.shape)
	preds = nnmodel.predict(X)
	preds_lstm = [np.argmax(i) for i in preds]
	
	clf_our = pickle.load(open(our_model,'rb'))
	clf_docemb = pickle.load(open(docemb_model,'rb'))
	clf_final = pickle.load(open(final_model,'rb'))

	### OUR FEATURES CLASSIFIER ###
	print('Vectorizing with our features')
	X=[features.count(doc) for doc in documents]
	preds_our = clf_our.predict(X)

	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(word_vectors)

	### DOCUMENT EMBEDDINGS WITH SVM ###
	print('Vectorizing with article embedding')
	X=[doc_embeddings.article2emb(doc,modelwords) for doc in documents]
	preds_docemb = clf_docemb.predict(X)

	print('Training meta classifier')

	X_final=[]
	for i in range(len(preds_our)):
		array_preds = [preds_our[i], preds_docemb[i], preds_lstm[i]]
		X_final.append(array_preds)

	preds_final = clf_final.predict(X_final)

	with open(args.output_file,'w') as outf:
		for idx,p in enumerate(preds_final):
			doc_id = doc_ids[idx]
			if p == 1:
				pstr='true'
			else:
				pstr='false'
			outf.write(doc_id+'\t'+pstr+'\n')		
