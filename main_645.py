import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
from cnn import cnn_module
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
from sklearn.metrics import classification_report

def save_tokenizer(tokenizer, outpath):
	with open(outpath, 'wb') as handle:
	    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
	print('Tokenizer: ',tokenizer,' | Saved to: ',outpath)

def train_cnn(all_articles, all_labels, MAXLEN, tokenizer_path='vectorizers/cnn_tokenizer.pkl', model_path='models/cnn_model', epochs=10, batch_size=1000):
	# Vectorize dataset
	tokenizer, X, y = cnn_module.vectorize(all_articles, all_labels, MAXLEN, tokenizer_path)
	# Save tokenizer to disk (needed for preprocessing validation/test data)
	save_tokenizer(tokenizer, tokenizer_path)
	# Load word embeddings
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(epath)
	# Create embedding layer
	embedding_layer=cnn_module.make_embedding_layer(tokenizer,dimwords,vocabwords,modelwords,MAXLEN)
	# Create cnn model
	nnmodel=cnn_module.build_model(embedding_layer)
	# Fit cnn model
	epochs=epochs
	batch_size=batch_size
	nnmodel.fit(X,y,epochs=epochs,batch_size=batch_size)
	# Save cnn model
	nnmodel.save(model_path)
	return nnmodel

def predict_with_cnn(tokenizer, cnn_model, input_dataset, *args):
	"""
	TO-DO: Use a saved keras tokenizers (on training by-publisher data) and simple cnn_model and make predictions on text
	TO-DO: Move a main_predict.py
	"""
	# X_test = vectorize_test(input_dataset, tokenizer)
	# predictions = cnn_model.predict(X_test)
	# pretty_predictions = format_predictions(predictions) # format predictions for official evaluator script
	# return pretty_predictions
	pass

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-itrain','--train-file', help='XML train file', required=True)
	parser.add_argument('-otrain','--output-train-file', help='Cleaned output train file', required=True)
	parser.add_argument('-ltrain','--train-label-file', help='Label train file', required=False)

	parser.add_argument('-wv','--word-vectors', help='Word vectors', required=True)

	parser.add_argument('-mxw','--maxlen', help='Maximum doc length (in words)', required=True)
	parser.add_argument('-t','--tokenizer-path', help='Tokenizer path', required=True)
	parser.add_argument('-m','--model', help='Model path', required=True)


	args = parser.parse_args()

	print('=== Processing Training Data ===\n')
	train_data,train_labels=article_processor(args.train_file,args.output_train_file,args.train_label_file)	
	all_train_articles=[]
	all_train_labels=[]
	for doc_id,document in train_data.items():
		all_train_articles.append(document.lower())#lower case the document!
		all_train_labels.append(train_labels[doc_id])

	docs_train, docs_test, y_train, y_test = train_test_split(all_train_articles,all_train_labels,test_size=0.3,random_state=1)
	docs_val, docs_test, y_val, y_test = train_test_split(docs_test,y_test,test_size=0.5,random_state=2)

	tokenizer_path=args.tokenizer_path+'_maxlen='+args.maxlen+'.pkl'
	model_path = args.model+'_maxlen='+args.maxlen+'.pkl'

	print('=== Vectorizing Training Data ===\n')
	tokenizer, X_train, y_train = cnn_module.vectorize(docs_train, y_train, int(args.maxlen), tokenizer_path)
	# Save tokenizer to disk (needed for preprocessing validation/test data)
	save_tokenizer(tokenizer, tokenizer_path)
	
	print('=== Vectorizing Validation Data ===\n')
	_ , X_val, y_val = cnn_module.vectorize(docs_val, y_val, int(args.maxlen), tokenizer_path)
	print('=== Vectorizing Test Data ===\n')
	_ , X_test, y_test = cnn_module.vectorize(docs_test, y_test, int(args.maxlen), tokenizer_path)

	print('=== Verifying tensor shapes ===')
	print('Train data shapes: ',X_train.shape,' | ',y_train.shape)
	print('Val data shapes: ',X_val.shape,' | ',y_val.shape)
	print('Test data shapes: ',X_test.shape,' | ',y_test.shape)

	print('=== Loading Embeddings and Creating Embedding Layer ===\n')
	# Load word embeddings
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(args.word_vectors)
	# Create embedding layer
	embedding_layer=cnn_module.make_embedding_layer(tokenizer,dimwords,vocabwords,modelwords,int(args.maxlen))

	# Create cnn model
	filters=100
	kernel_size=5
	pool_size=4
	strides=3
	padding='valid'
	activation='relu'
	nnmodel = Sequential()
	nnmodel.add(embedding_layer)
	nnmodel.add(Conv1D(filters,
	                 kernel_size,
	                 padding=padding,
	                 activation=activation,
	                 strides=strides))
	nnmodel.add(MaxPooling1D(pool_size=pool_size))
	nnmodel.add(Bidirectional(LSTM(100)))
	nnmodel.add(Dropout(0.25))
	# We add a vanilla hidden layer:
	nnmodel.add(Dense(100))
	nnmodel.add(Dropout(0.2))
	nnmodel.add(Activation('relu'))	
	nnmodel.add(Dense(2))
	nnmodel.add(Activation('softmax'))
	nnmodel.compile(loss='binary_crossentropy',
	              optimizer='adagrad',
	              metrics=['accuracy'])
	print('=== Model Summary ===')
	print(nnmodel.summary())

	epochs=100
	batch_size=10
	nnmodel.fit(X_train,y_train,epochs=epochs,batch_size=batch_size, validation_data=(X_val, y_val))

	preds=nnmodel.predict_classes(X_test)
	print(classification_report([np.argmax(i) for i in y_test], preds))

	# Save cnn model
	nnmodel.save(model_path)