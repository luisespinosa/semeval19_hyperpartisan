import sys
import os
from sklearn.model_selection import train_test_split
from data_manager.read_dataset import *
import pipeline
from embeddings import embedding_handler
import pickle
from argparse import ArgumentParser
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Dropout, Embedding, ActivityRegularization, concatenate
from keras.models import Model, Sequential
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.svm import SVC
import doc_embeddings
import delete_quotes as dq
import features

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-itrain','--input-file', help='XML train file', required=True)
	parser.add_argument('-otrain','--output-file', help='Cleaned output train file', required=True)
	parser.add_argument('-ltrain','--label-file', help='Label train file', required=False)

	parser.add_argument('-wv','--word-vectors', help='Word vectors', required=True)

	args=parser.parse_args()

	# LOAD DATA
	documents,labels,_=pipeline.load_xml_files(args.input_file, args.output_file, args.label_file, delete_quotes=True)	
	# LOAD EMBEDDINGS
	modelwords, vocabwords, dimwords = embedding_handler.load_embeddings(args.word_vectors)

	X_train=[]
	for doc in documents:
		doc_emb=doc_embeddings.article2emb(doc, modelwords)
		features_vec=features.count(doc)
		X_train.append(np.concatenate([doc_emb,features_vec]))

	clf = SVC(kernel='linear',probability=True).fit(X_train, labels)

	print('Now save models to file')
	model_path = os.path.join('models',clf.__repr__().split('(')[0]+'_combined.clf')
	pickle.dump(clf,open(model_path,'wb'),protocol=pickle.HIGHEST_PROTOCOL)




