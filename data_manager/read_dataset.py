#!/usr/bin/env python

"""Term frequency extractor for the PAN19 hyperpartisan news detection task"""
# Version: 2018-11-23

# Parameters:
# --inputDataset=<directory>
#   Directory that contains the articles XML file with the articles for which a prediction should be made.
# --outputFile=<file>
#   File to which the term frequency vectors will be written. Will be overwritten if it exists.

# Output is one article per line:
# <article id> <token>:<count> <token>:<count> ...


import os
import getopt
import sys
import xml.sax
import lxml.sax
import lxml.etree
import re
from nltk.tokenize import word_tokenize,sent_tokenize
import xml.dom.minidom as minidom
from collections import defaultdict

########## OPTIONS HANDLING ##########
def parse_options():
    """Parses the command line options."""
    try:
        long_options = ["inputDataset=", "outputFile="]
        opts, _ = getopt.getopt(sys.argv[1:], "d:o:", long_options)
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    inputDataset = "undefined"
    outputFile = "undefined"

    for opt, arg in opts:
        if opt in ("-d", "--inputDataset"):
            inputDataset = arg
        elif opt in ("-o", "--outputFile"):
            outputFile = arg
        else:
            assert False, "Unknown option."
    if inputDataset == "undefined":
        sys.exit("Input dataset, the directory that contains the articles XML file, is undefined. Use option -d or --inputDataset.")
    elif not os.path.exists(inputDataset):
        sys.exit("The input dataset folder does not exist (%s)." % inputDataset)

    if outputFile == "undefined":
        sys.exit("Output file, the file to which the vectors should be written, is undefined. Use option -o or --outputFile.")

    return (inputDataset, outputFile)



########## ARTICLE HANDLING ##########
def handleArticle(article, outFile):
    termfrequencies = {}

    # get text from article
    text = lxml.etree.tostring(article, method="text", encoding="unicode")
    #textcleaned = re.sub('[^a-z ]', '', text.lower())
    sents = sent_tokenize(text)

    art_id=article.get("id")

    for sent in sents:
        tokens = word_tokenize(sent)
        outFile.write(art_id+'\t'+' '.join(tokens)+'\n')

########## SAX FOR STREAM PARSING ##########
class HyperpartisanNewsTFExtractor(xml.sax.ContentHandler):
    def __init__(self, outFile):
        xml.sax.ContentHandler.__init__(self)
        self.outFile = outFile
        self.lxmlhandler = "undefined"

    def startElement(self, name, attrs):
        if name != "articles":
            if name == "article":
                self.lxmlhandler = lxml.sax.ElementTreeContentHandler()

            self.lxmlhandler.startElement(name, attrs)

    def characters(self, data):
        if self.lxmlhandler != "undefined":
            self.lxmlhandler.characters(data)

    def endElement(self, name):
        if self.lxmlhandler != "undefined":
            self.lxmlhandler.endElement(name)
            if name == "article":
                # pass to handleArticle function
                handleArticle(self.lxmlhandler.etree.getroot(), self.outFile)
                self.lxmlhandler = "undefined"
            


########## MAIN ##########
def article_processor(inputDataset, outputFile, label_file=False):
    """Main method of this module."""

    if not os.path.exists(outputFile):
        print('Cleaning xml file')
        with open(outputFile, 'w', encoding='utf-8') as outFile:
            for file in os.listdir(inputDataset):
                if file.endswith(".xml") and not file.startswith('ground'):
                    ifile=inputDataset + "/" + file
                    with open(ifile, 'r', encoding='utf-8') as inputRunFile:
                        parser = xml.sax.make_parser()
                        parser.setContentHandler(HyperpartisanNewsTFExtractor(outFile))
                        source = xml.sax.xmlreader.InputSource()
                        source.setByteStream(inputRunFile)
                        source.setEncoding("utf-8")
                        parser.parse(source)
        print('New preprocesed file saved in : ',outputFile)
    else:
        print('Using already cleaned file ',outputFile,' because it exists in disk')

    print('Reading cleaned data file')
    data=read_preprocessed(outputFile)

    if label_file:
        print('Reading labels')
        labels=read_labels(label_file)
        print(len(data),len(labels))
        return data,labels
    else:
        print(len(data))
        return data,None


def read_labels(label_file):
    def translate_tag(tag):
        if tag=='true':
            return 1
        else:
            return 0
    dom = minidom.parse(open(label_file))
    root=dom.childNodes[0]
    labels={}
    for node in root.childNodes:
        if node.nodeName=='article':
            metadata=node.attributes.items()
            d={}
            for key, value in metadata:
                d[key]=value
            labels[d['id']]=translate_tag(d['hyperpartisan'])    
    return labels

def read_preprocessed(clean_file):
    d=defaultdict(str)
    with open(clean_file,'r',encoding='utf-8') as f:
        for line in f:
            #print(line)
            if line:
                cols=line.split('\t')
                doc_id=cols[0]
                if len(doc_id) == 7:
                    #doc_id=int(doc_id)
                    content=cols[1]
                else:
                    content=line.strip()
                d[doc_id]+=content
    return d
if __name__ == '__main__':
    main(*parse_options())

