#!/usr/bin/env python

"""Term frequency extractor for the PAN19 hyperpartisan news detection task"""
# Version: 2018-11-23

# Parameters:
# --inputDataset=<directory>
#   Directory that contains the articles XML file with the articles for which a prediction should be made.
# --outputFile=<file>
#   File to which the term frequency vectors will be written. Will be overwritten if it exists.

# Output is one article per line:
# <article id> <token>:<count> <token>:<count> ...


import os
import getopt
import sys
import xml.sax
import lxml.sax
import lxml.etree
import re
from nltk.tokenize import word_tokenize,sent_tokenize


########## OPTIONS HANDLING ##########
def parse_options():
    """Parses the command line options."""
    try:
        long_options = ["inputDataset=", "outputFile="]
        opts, _ = getopt.getopt(sys.argv[1:], "d:o:", long_options)
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    inputDataset = "undefined"
    outputFile = "undefined"

    for opt, arg in opts:
        if opt in ("-d", "--inputDataset"):
            inputDataset = arg
        elif opt in ("-o", "--outputFile"):
            outputFile = arg
        else:
            assert False, "Unknown option."
    if inputDataset == "undefined":
        sys.exit("Input dataset, the directory that contains the articles XML file, is undefined. Use option -d or --inputDataset.")
    elif not os.path.exists(inputDataset):
        sys.exit("The input dataset folder does not exist (%s)." % inputDataset)

    if outputFile == "undefined":
        sys.exit("Output file, the file to which the vectors should be written, is undefined. Use option -o or --outputFile.")

    return (inputDataset, outputFile)



########## ARTICLE HANDLING ##########
def handleArticle(article):

    # get text from article
    text = lxml.etree.tostring(article, method="text", encoding="unicode")
    #sents=sent_tokenize(text)

    yield text,article.get('id')
    input('---')
    # writing counts: <article id> <token>:<count> <token>:<count> ...
    #for sent in sents:
    #    print(article.get("id"),'->',article.get('hyperpartisan'),'->',sent)



########## SAX FOR STREAM PARSING ##########
class HyperpartisanNewsTFExtractor(xml.sax.ContentHandler):
    def __init__(self):
        xml.sax.ContentHandler.__init__(self)
        #self.outFile = outFile
        self.lxmlhandler = "undefined"

    def startElement(self, name, attrs):
        if name != "articles":
            if name == "article":
                self.lxmlhandler = lxml.sax.ElementTreeContentHandler()

            self.lxmlhandler.startElement(name, attrs)

    def characters(self, data):
        if self.lxmlhandler != "undefined":
            self.lxmlhandler.characters(data)

    def endElement(self, name):
        if self.lxmlhandler != "undefined":
            self.lxmlhandler.endElement(name)
            if name == "article":
                # pass to handleArticle function
                handleArticle(self.lxmlhandler.etree.getroot())
                self.lxmlhandler = "undefined"
            


########## MAIN ##########
def main(inputDataset):
    """Main method of this module."""

    with open(inputDataset, 'r') as inputRunFile:
        print('1')
        parser = xml.sax.make_parser()
        print('2')
        parser.setContentHandler(HyperpartisanNewsTFExtractor())
        print('3')
        source = xml.sax.xmlreader.InputSource()
        print('4')
        source.setByteStream(inputRunFile)
        print('5')
        source.setEncoding("utf-8")
        print('6')
        parser.parse(source)
        return parser
        print('7')


if __name__ == '__main__':
    main(*parse_options())

