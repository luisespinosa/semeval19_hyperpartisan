import re

QUOTATION_MARKS=[('``',"''"),("“","”"), ('"','"')]

def delete_quotes(document_string, delimiters, min_quote_len = 3):
	for start,end in delimiters:
		p=re.compile(start+"(.*?)"+end)
		match_list = re.findall(p,document_string)
		if match_list:
			for match in match_list:
				if len(match.split()) > min_quote_len:
					document_string=document_string.replace(match,'')
	return document_string